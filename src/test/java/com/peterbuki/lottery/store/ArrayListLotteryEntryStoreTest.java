package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;
import com.peterbuki.lottery.entry.LotteryEntryBuilder;
import com.peterbuki.lottery.entry.LotteryException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ArrayListLotteryEntryStoreTest {

    ArrayListLotteryEntryStore store = new ArrayListLotteryEntryStore();

    @Test
    public void addEntry_add1Entry_contains1Entries() throws LotteryException {
        store.addEntry(LotteryEntryBuilder.fromArray(1, 2, 3, 4, 5));
        assertEquals(1, (long) store.getNumberOfEntries());
    }

    @Test
    public void findMatchingEntries_existingEntry_foundsIt() throws LotteryException {
        store.addEntry(LotteryEntryBuilder.fromArray(1, 2, 3, 4, 5));
        store.addEntry(LotteryEntryBuilder.fromArray(6, 7, 3, 9, 10));
        store.addEntry(LotteryEntryBuilder.fromArray(11, 12, 3, 14, 15));
        store.addEntry(LotteryEntryBuilder.fromArray(16, 17, 3, 1, 20));
        store.addEntry(LotteryEntryBuilder.fromArray(21, 22, 3, 24, 2));

        List<LotteryEntry> results = store.findMatchingEntries(
                LotteryEntryBuilder.fromArray(2, 3, 67, 68, 69), 2);
        assertEquals(2, results.size());
    }
}