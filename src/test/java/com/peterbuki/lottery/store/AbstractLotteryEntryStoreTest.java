package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;
import com.peterbuki.lottery.entry.LotteryEntryBuilder;
import com.peterbuki.lottery.entry.LotteryException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AbstractLotteryEntryStoreTest {
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    AbstractLotteryEntryStore store;

    @Test
    public void addEntries_add3Entries_Contains3Entries() throws LotteryException {
        LotteryEntry e1 = LotteryEntryBuilder.fromArray(1, 2, 3, 4, 5);
        LotteryEntry e2 = LotteryEntryBuilder.fromArray(21, 2, 3, 4, 5);
        LotteryEntry e3 = LotteryEntryBuilder.fromArray(31, 2, 3, 4, 5);

        store.addEntries(new LotteryEntry[]{e1, e2, e3});
        Mockito.verify(store, times(3)).addEntry(any(LotteryEntry.class));
    }
}