package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;
import com.peterbuki.lottery.entry.LotteryEntryBuilder;
import com.peterbuki.lottery.main.LotteryConfig;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class FileBasedLotteryEntryStorePopulatorTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private FileBasedLotteryEntryStorePopulator storePopulator;

    @Mock
    LotteryEntryStore store;

    private static final String FILENAME_INVALID_ENTRY = "src/test/resources/invalid-entries.txt";

    @Test
    public void readLotteryEntriesFromFile_fileMissing_throwsException() throws Exception {
        exception.expect(IOException.class);

        storePopulator = new FileBasedLotteryEntryStorePopulator("missingfile");
    }

    @Test
    public void readLotteryEntriesFromFile_fileWithInvalidEntry_givesWarning() throws Exception {
        LotteryEntryBuilder.setLotteryConfig(LotteryConfig.createDefaultLotteryConfig());

        PrintStream originalErrorStream = System.err;
        OutputStream err = new ByteArrayOutputStream();
        System.setErr(new PrintStream(err));

        storePopulator = new FileBasedLotteryEntryStorePopulator(FILENAME_INVALID_ENTRY);
        storePopulator.readLotteryEntriesFromFile(store);

        String expectedError = "Warning: skipping invalid entry: -10 2 41 1 0" + System.getProperty("line.separator") +
                "Warning: skipping invalid entry: 1 2 3 4" + System.getProperty("line.separator") +
                "Warning: skipping invalid entry: 1 2 3 4 5 6" + System.getProperty("line.separator");

        assertEquals(expectedError, err.toString());

        Mockito.verify(store, times(2)).addEntry(any(LotteryEntry.class));
        System.setErr(originalErrorStream);
    }

}