package com.peterbuki.lottery.util;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ProgressBarTest {
    private String LINESEPARATOR = System.getProperty("line.separator");
    private ProgressBar progressBar;
    private OutputStream out;

    @Before
    public void setUp() {
        out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);

        progressBar = new ProgressBar(printStream);
    }

    @Test
    public void integrationTest_3StepsUpdate() {
        progressBar.setSleepTime(100);
        progressBar.setMaxStatus(100);
        progressBar.setLineLength(50);
        progressBar.start();
        sleep(110);
        progressBar.setCurrentStatus(49);
        sleep(110);
        progressBar.setCurrentStatus(99);
        sleep(110);
        progressBar.setCurrentStatus(100);
        progressBar.stop();

        String expectedOutput = "\r" +
                "[...........................................]  0%\r" +
                "[...........................................]  0%\r" +
                "[########################...................] 49%\r" +
                "[###########################################] 99%\r" +
                "[###########################################] 100%" + LINESEPARATOR;
        assertEquals(expectedOutput, out.toString());

    }

    private void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}