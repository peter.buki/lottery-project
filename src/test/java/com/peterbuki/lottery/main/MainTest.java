package com.peterbuki.lottery.main;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class MainTest {
    public static final String LINESEPARATOR = System.getProperty("line.separator");
    private PrintStream originalOut = System.out;
    private InputStream in;
    ByteArrayOutputStream out;

    @Before
    public void setup() {
        out = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(out);
        System.setOut(printStream);
    }

    @Test
    public void main_5entries_singleMatch() throws Exception {
        in = new ByteArrayInputStream("4 79 13 80 56\n".getBytes());
        System.setIn(in);

        System.err.println("Working Directory = " +
                System.getProperty("user.dir"));
        Main.main(new String[]{"src/test/resources/5-entries.txt"});
        String expectedLines =
                "[#########################################################################] 100%" + LINESEPARATOR +
                        "Ready> " +
                        "Numbers Matching    Winners" + LINESEPARATOR +
                        "5                   1" + LINESEPARATOR +
                        "4                   1" + LINESEPARATOR +
                        "3                   1" + LINESEPARATOR +
                        "2                   1" + LINESEPARATOR +
                        "Ready> ";
        int beginPosition = out.size() - expectedLines.length();
        assertEquals(
                expectedLines, out.toString().substring(beginPosition));
    }

    @Test
    public void main_5entries_twoMatches() throws Exception {
        in = new ByteArrayInputStream("53 12 2 31 34\n".getBytes());
        System.setIn(in);

        System.err.println("Working Directory = " +
                System.getProperty("user.dir"));
        Main.main(new String[]{"src/test/resources/5-entries.txt"});
        String expectedLines =
                "[#########################################################################] 100%" + LINESEPARATOR +
                        "Ready> " +
                        "Numbers Matching    Winners" + LINESEPARATOR +
                        "5                   2" + LINESEPARATOR +
                        "4                   0" + LINESEPARATOR +
                        "3                   0" + LINESEPARATOR +
                        "2                   0" + LINESEPARATOR +
                        "Ready> ";
        int beginPosition = out.size() - expectedLines.length();
        assertEquals(
                expectedLines, out.toString().substring(beginPosition));
    }

    @Test
    public void main_noArgument_printsUsage() throws Exception {
        Main.main(new String[]{});

        String expectedLines = "Usage: lottery <input file>\n" +
                "input file must contain lottery entries separated by space." +
                "Example contents:\n" +
                "1 2 3 4 5\n";
        assertEquals(expectedLines, out.toString());
    }

    @After
    public void cleanup() {
        System.setOut(originalOut);
    }
}