package com.peterbuki.lottery.entry;

import org.junit.Test;

import static org.junit.Assert.*;

public class LotteryEntryTest {


    @Test
    public void equals_theyAreEqual_returnsTrue() throws LotteryException {
        LotteryEntry lotteryEntry1 = LotteryEntryBuilder.fromArray(3, 2, 4, 1, 5);
        LotteryEntry lotteryEntry2 = LotteryEntryBuilder.fromArray(5, 4, 3, 2 ,1);
        assertTrue(((Object) lotteryEntry1).equals(lotteryEntry2));
    }

    @Test
    public void equals_theyDiffer_returnsFalse() throws  LotteryException {
        LotteryEntry lotteryEntry1 = LotteryEntryBuilder.fromArray(3, 2, 74, 1, 5);
        LotteryEntry lotteryEntry2 = LotteryEntryBuilder.fromArray(5, 4, 3, 2 ,1);
        assertFalse(lotteryEntry1.equals(lotteryEntry2));
    }

    @Test
    public void equals_differentObject_returnsFalse() throws LotteryException {
        LotteryEntry lotteryEntry1 = LotteryEntryBuilder.fromArray(3,2,74,1,5);
        assertFalse(lotteryEntry1.equals(""));
    }

    @Test
    public void getNumbers_returnCorrectNumbers() throws LotteryException {
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromArray(3,2,74,1,5);
        Integer[] expectedNumbers = new Integer[]{1, 2, 3, 5, 74};
        assertArrayEquals(expectedNumbers, lotteryEntry.getNumbers());
    }

    @Test
    public void equalEntries_with2IdenticalNumbers_returns2() throws LotteryException {
        LotteryEntry lotteryEntry1 = LotteryEntryBuilder.fromArray(12,34,56,78,90);
        LotteryEntry lotteryEntry2 = LotteryEntryBuilder.fromArray(89, 78, 34, 11 ,1);
        assertEquals(2, lotteryEntry1.equalEntries(lotteryEntry2));
    }

}