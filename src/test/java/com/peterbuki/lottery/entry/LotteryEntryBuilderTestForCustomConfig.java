package com.peterbuki.lottery.entry;

import com.peterbuki.lottery.main.LotteryConfig;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class LotteryEntryBuilderTestForCustomConfig {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @BeforeClass
    public static void setup() throws LotteryException {
        LotteryConfig config = LotteryConfig.createCustomLotteryConfig(1, 45, 6, 3);
        LotteryEntryBuilder.setLotteryConfig(config);
    }

    @Test
    public void toString_createdWrongOrder() throws LotteryException {
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromArray(3, 2, 4, 1, 5, 6);
        String expected = "Entry: 1 2 3 4 5 6";
        assertEquals(expected, lotteryEntry.toString());
    }

    @Test
    public void toString_createdWithString() throws LotteryException {
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("6 5 4 1 2 3");
        String expected = "Entry: 1 2 3 4 5 6";
        assertEquals(expected, lotteryEntry.toString());
    }

    @Test
    public void toString_invalidEntry5Numbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 5");
    }

    @Test
    public void toString_invalidEntry7Numbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 5 6 7");
    }

    @Test
    public void toString_invalidEntryTooHighNumbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 5 46");
    }




}
