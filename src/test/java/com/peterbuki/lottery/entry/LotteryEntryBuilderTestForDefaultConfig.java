package com.peterbuki.lottery.entry;

import com.peterbuki.lottery.main.LotteryConfig;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class LotteryEntryBuilderTestForDefaultConfig {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @BeforeClass
    public static void setup() throws Exception {
        LotteryConfig config = LotteryConfig.createDefaultLotteryConfig();
        LotteryEntryBuilder.setLotteryConfig(config);
    }

    @Test
    public void toString_createdWrongOrder() throws LotteryException {
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromArray(3, 2, 4, 1, 5);
        String expected = "Entry: 1 2 3 4 5";
        assertEquals(expected, lotteryEntry.toString());
    }

    @Test
    public void fromArray_invalidEntryDuplicatedNumbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromArray(1, 2, 3, 4, 5, 1);
    }

    @Test
    public void fromString_invalidEntryDuplicatedNumbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 5 1");
    }

    @Test
    public void toString_createdWithString() throws LotteryException {
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("5 4 1 2 3");
        String expected = "Entry: 1 2 3 4 5";
        assertEquals(expected, lotteryEntry.toString());
    }

    @Test
    public void toString_invalidEntry4Numbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 ");
    }

    @Test
    public void toString_invalidEntry6Numbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 5 6");
    }

    @Test
    public void toString_invalidEntryTooHighNumbers_throwsException() throws LotteryException {
        exception.expect(LotteryException.class);
        exception.expectMessage("Invalid input");
        LotteryEntry lotteryEntry = LotteryEntryBuilder.fromString("1 2 3 4 91");
    }

}