package com.peterbuki.lottery;

import com.peterbuki.lottery.main.Main;
import com.peterbuki.lottery.util.PerformanceTimer;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.Thread.sleep;

public class PerformanceTests {
    // Filename for the test data
    private static final String FILENAME_10K = "src/test/resources/10k-entries.txt";
    private static final String FILENAME_100K = "src/test/resources/100k-entries.txt";
    private static final String FILENAME_1M = "src/test/resources/1m-entries.txt";
    private static final String FILENAME_5M = "src/test/resources/5m-entries.txt";
    private static final String FILENAME_10M = "src/test/resources/10m-entries.txt";

    private PerformanceTimer timer;
    private PrintStream originalOut;
    private InputStream inputStream;
    private OutputStream outputStream;
    private OutputStream errorStream;
    private String filename;

    @Before
    public void setup() throws Exception {
        System.gc();
        sleep(5000);
        System.out.println(String.format("Memory usage: %dMB", getMemoryUsage()));

        originalOut = System.out;

        inputStream = new ByteArrayInputStream(".\n".getBytes());
        System.setIn(inputStream);

        outputStream = new ByteArrayOutputStream();
        PrintStream standardOutput = new PrintStream(outputStream);
        System.setOut(standardOutput);

        errorStream = new ByteArrayOutputStream();
        PrintStream errorOutput = new PrintStream(errorStream);
        System.setErr(errorOutput);

        timer = new PerformanceTimer();
    }

    @Test
    public void arrayListLotteryEntryStore_load10k_performanceTest() throws Exception {
        filename = FILENAME_10K;
        runOneRoundUsingMain();
    }

    @Test
    @Ignore
    public void arrayListLotteryEntryStore_load100k_performanceTest() throws Exception {
        filename = FILENAME_100K;
        runOneRoundUsingMain();
    }

    @Test
    @Ignore
    public void arrayListLotteryEntryStore_load1m_performanceTest() throws Exception {
        filename = FILENAME_1M;
        runOneRoundUsingMain();
    }

    @Test
    @Ignore
    public void arrayListLotteryEntryStore_load5m_performanceTest() throws Exception {
        filename = FILENAME_5M;
        runOneRoundUsingMain();
    }

    @Test
    @Ignore
    public void arrayListLotteryEntryStore_load10m_performanceTest() throws Exception {
        filename = FILENAME_10M;
        runOneRoundUsingMain();
    }

    private void runOneRoundUsingMain() throws Exception {
        Main.main(new String[]{filename});
    }

    @After
    public void printSummary() throws IOException {
        long elapsed = timer.getElapsed();
        long lineCount = Files.lines(Paths.get(filename)).count();
        long recordsPerSeconds = Math.round(lineCount * 1000 / elapsed);

        System.setOut(originalOut);

        long memoryUsage = getMemoryUsage();
        System.out.println(String.format("Read %d records in %dms, %d records/seconds, memory usage: %dMB",
                lineCount, elapsed, recordsPerSeconds, memoryUsage));

        printLineSeparator("Original output");
        System.out.println(outputStream.toString());

        printLineSeparator("Error output");
        System.out.println(errorStream.toString());
    }

    private long getMemoryUsage() {
        return Runtime.getRuntime().totalMemory() / 1024 / 1024;
    }

    private void printLineSeparator(String subject) {
        System.out.println("-------------------------- " + subject + " --------------------------");
    }
}
