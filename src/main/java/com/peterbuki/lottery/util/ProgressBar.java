package com.peterbuki.lottery.util;

import java.io.PrintStream;

/**
 * Progress bar implementation - prints and updates a progress bar to the given PrintStream
 */
public class ProgressBar {
    private static final int NUMBER_OF_SPARE_CHARACTERS = 7;
    private int sleepTimeInMilliSeconds = 100;
    private long currentStatus = 0;
    private long maxStatus = 100;
    private int lineLength = 80;
    private boolean running = false;
    private PrintStream outputStream;

    /**
     * Inner class to hide the method
     */
    private class Runner implements Runnable {
        @Override
        public void run() {
            while (running) {
                updateDisplay();
                sleep();
            }
        }
    }

    /**
     * Constructor
     * @param out the PrintStream for the output
     */
    public ProgressBar(PrintStream out) {
        this.outputStream = out;
    }

    /**
     * Starts the progress bar print and regular update
     */
    public void start() {
        running = true;
        new Thread(new Runner()).start();
    }

    /**
     * The maximum status - if current status reaches this point, the progress is 100%
     * @param maxStatus the maximum value
     */
    public void setMaxStatus(long maxStatus) {
        this.maxStatus = maxStatus;
    }

    /**
     * Updates current status - it will not update the progress bar!
     * @param currentStatus the current status value
     */
    public void setCurrentStatus(long currentStatus) {
        this.currentStatus = currentStatus;
    }

    /**
     * Line length for the progress bar to be shown
     * @param lineLength length of the line, by default 80
     */
    public void setLineLength(int lineLength) {
        this.lineLength = lineLength;
    }

    /**
     * The progress bar will be updated regularly according to this value
     * @param sleepTimeInMilliSeconds number of milliseconds to sleep between updates
     */
    public void setSleepTime(int sleepTimeInMilliSeconds) {
        this.sleepTimeInMilliSeconds = sleepTimeInMilliSeconds;
    }

    /**
     * Prints the progress bar according to the current status
     */
    private void updateDisplay() {
        int statusBricks = Math.round(currentStatus * lineLength / maxStatus);
        int percent = Math.round(currentStatus * 100 / maxStatus);
        StringBuilder progressBarLine = new StringBuilder("\r[");
        for (int i = 1; i <= lineLength - NUMBER_OF_SPARE_CHARACTERS; i++) {
            if (i <= statusBricks) {
                progressBarLine.append("#");
            } else {
                progressBarLine.append(".");
            }
        }
        progressBarLine.append(String.format("] %2d%%", percent));
        outputStream.print(progressBarLine.toString());
    }

    /**
     * Calling this method will stop the progress bar updates.
     */
    public void stop() {
        this.running = false;
        updateDisplay();
        outputStream.println();
    }


    /**
     * Internal method for sleeping between the updates.
     */
    private void sleep() {
        try {
            Thread.sleep(sleepTimeInMilliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
