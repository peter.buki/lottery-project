package com.peterbuki.lottery.util;

/**
 * Timer class - provides a simple feature: to check the time elapsed for the tests
 */
public class PerformanceTimer {

    private long timeStartedMillis;

    /**
     * Constructor: starts the timer
     */
    public PerformanceTimer() {
        reset();
    }

    /**
     * Resets the timer
     */
    public void reset() {
        timeStartedMillis = System.currentTimeMillis();
    }

    /**
     * Returns the time elapsed since last reset
     * @return time elapsed in milliseconds
     */
    public long getElapsed() {
        return System.currentTimeMillis() - timeStartedMillis;
    }
}
