package com.peterbuki.lottery.main;

import com.peterbuki.lottery.entry.LotteryException;

/**
 * Contains the configuration
 */
public class LotteryConfig {
    private static final int DEFAULT_ENTRY_NUMBER_MINIMUM_VALUE = 1;
    private static final int DEFAULT_ENTRY_NUMBER_MAXIMUM_VALUE = 90;
    private static final int DEFAULT_ENTRY_NUMBER_OF_NUMBERS = 5;
    private static final int DEFAULT_MINIMUM_MATCHES = 2;

    private final int minimumValue;
    private final int maximumValue;
    private final int numberOfNumbers;
    private final int minimumMatches;

    private static LotteryConfig lotteryConfig;

    /**
     * Default constructor with default values
     */
    private LotteryConfig() {
        lotteryConfig = this;
        this.minimumValue = DEFAULT_ENTRY_NUMBER_MINIMUM_VALUE;
        this.maximumValue = DEFAULT_ENTRY_NUMBER_MAXIMUM_VALUE;
        this.numberOfNumbers = DEFAULT_ENTRY_NUMBER_OF_NUMBERS;
        this.minimumMatches = DEFAULT_MINIMUM_MATCHES;
    }

    /**
     * Constructor for customized values
     */
    private LotteryConfig(int minimumValue, int maximumValue, int numberOfNumbers, int minimumMatches) {
        this.minimumValue = minimumValue;
        this.maximumValue = maximumValue;
        this.numberOfNumbers = numberOfNumbers;
        this.minimumMatches = minimumMatches;
    }

    /**
     * Creates LotteryConfig with default values
     *
     * @return the configuration created.
     */
    public static LotteryConfig createDefaultLotteryConfig() throws LotteryException {
        return new LotteryConfig();
    }

    /**
     * Allows creating custom configuration.
     *
     * @param minimumValue    minimumValue, by default 1
     * @param maximumValue    maximumValue, by default 90
     * @param numberOfNumbers number of numbers, by default 5
     * @param minimumMatches  minimum matching numbers, by default 2
     * @return The configuration.
     * @throws LotteryException If configuration has been already created before.
     */
    public static LotteryConfig createCustomLotteryConfig(int minimumValue, int maximumValue, int numberOfNumbers, int minimumMatches) throws LotteryException {
        return new LotteryConfig(minimumValue, maximumValue, numberOfNumbers, minimumMatches);
    }

    /**
     * Singleton: returning the single instance - it should have been created before.
     */
    public static LotteryConfig getLotteryConfig() {
        if (null != lotteryConfig) {
            lotteryConfig = new LotteryConfig();
            System.err.println("Warning, no configuration, creating with default values.");
        }
        return lotteryConfig;
    }

    /**
     * The minimum value of numbers in an entry
     *
     * @return minimum value, by default 1
     */
    public int getMinimumValue() {
        return minimumValue;
    }

    /**
     * The maximum value of numbers in an entry
     *
     * @return maximum value, by default 90
     */
    public int getMaximumValue() {
        return maximumValue;
    }

    /**
     * The number of numbers in an entry
     *
     * @return number of numbers, by default 5
     */
    public int getNumberOfNumbers() {
        return numberOfNumbers;
    }

    /**
     * The number of minimum matches to find
     *
     * @return minimum matches, by default 2
     */
    public int getMinimumMatches() {
        return minimumMatches;
    }


}
