package com.peterbuki.lottery.main;

import java.io.IOException;

public class Main {
    /**
     * Main method
     *
     * @param args a filename that contains the entries
     * @throws IOException if file is missing or not readable
     */
    public static void main(String[] args) throws Exception {
        LotteryApp lotteryApp = new LotteryApp();

        // check arguments - only accept 1 parameter, a filename
        if (args.length == 1) {
            lotteryApp.setFilename(args[0]);
        } else {
            String usage = "Usage: lottery <input file>\n" +
                    "input file must contain lottery entries separated by space." +
                    "Example contents:\n" +
                    "1 2 3 4 5\n";
            System.out.print(usage);
            return;
        }

        // set the configuration, example for 6-45 lottery schema:
        //LotteryConfig config = LotteryConfig.createCustomLotteryConfig(1, 45, 6, 3);
        LotteryConfig config = LotteryConfig.createDefaultLotteryConfig();

        lotteryApp.setLotteryConfig(config);
        lotteryApp.setupInterruptHandler();

        // read entries from file
        lotteryApp.readLotteryEntries();
        lotteryApp.handleUserInputForSearch();
    }
}

