package com.peterbuki.lottery.main;

import com.peterbuki.lottery.entry.LotteryEntry;
import com.peterbuki.lottery.entry.LotteryEntryBuilder;
import com.peterbuki.lottery.entry.LotteryException;
import com.peterbuki.lottery.store.ArrayListLotteryEntryStore;
import com.peterbuki.lottery.store.FileBasedLotteryEntryStorePopulator;
import com.peterbuki.lottery.store.LotteryEntryStore;
import com.peterbuki.lottery.store.LotteryEntryStorePopulator;
import com.peterbuki.lottery.util.PerformanceTimer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Main class, validates arguments, reads file into a store and handles user input
 */
public class LotteryApp {

    private LotteryEntryStore store = new ArrayListLotteryEntryStore();
    private PerformanceTimer timer = new PerformanceTimer();
    private LotteryEntryStorePopulator storePopulator;
    private LotteryConfig config;

    private boolean running = true;

    /**
     * Sets the file name for the lottery entries
     *
     * @param filename name of the file with the lottery entries
     * @throws IOException if the file is missing or not readable
     */
    public void setFilename(String filename) throws IOException {
        storePopulator = new FileBasedLotteryEntryStorePopulator(filename);
        storePopulator.setLotteryConfig(config);
    }

    /**
     * Populates the LotteryEntry tore with numbers from the file
     *
     * @throws IOException if file is not readable
     */
    public void readLotteryEntries() throws IOException {
        timer.reset();
        storePopulator.readLotteryEntriesFromFile(store);
        long elapsed = (timer.getElapsed() == 0) ? 1 : timer.getElapsed();
        long recordCount = store.getStoredRecordCount();
        System.err.println(String.format("Loaded %d entries in %dms, that's %d entry/seconds!",
                recordCount, elapsed, recordCount * 1000 / elapsed));
    }

    /**
     * Prints READY> prompt and waits for user input
     * Parses each line the user enters, finds the matching entries and prints the table
     *
     * @throws IOException if input is broken
     */
    public void handleUserInputForSearch() throws IOException {
        // main loop - get input and check results
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (running) {
            printPromptToConsole();
            String line = reader.readLine();
            if (userWantsToExit(line)) {
                break;
            }

            try {
                // Try to create an entry to find in the store
                LotteryEntry winnerNumbers = LotteryEntryBuilder.fromString(line);

                timer.reset();
                // Find the winners in the LotteryEntryStore
                List<LotteryEntry> winners = store.findMatchingEntries(winnerNumbers, config.getMinimumMatches());
                long elapsed = (timer.getElapsed() == 0) ? 1 : timer.getElapsed();
                System.err.println(String.format("Found %d matching entries in %dms, that's %d entry/seconds.",
                        winners.size(), elapsed, store.getStoredRecordCount() * 1000 / elapsed));
                // Create winners table. winnerTable[5] will have the number of 5 matches
                int[] winnerTable = createWinnersTable(winnerNumbers, winners);

                // Create human readable string from the winner table
                StringBuilder outputString = createHumanReadableWinnersTable(winnerTable);

                // Print it to the screen
                System.out.print(outputString.toString());
            } catch (LotteryException lotteryException) {
                System.err.println(lotteryException.getMessage());
            }
        }
    }

    /**
     * Creates an array with the number of winners
     * The winnerTable[5] will contain the number of 5 matches
     *
     * @param winnerNumbers the entry with the winning numbers
     * @param winners       list of the winner entries
     * @return an array with the number of winners
     */
    private int[] createWinnersTable(LotteryEntry winnerNumbers, List<LotteryEntry> winners) {
        // we store the winning entries in the slot of number of matches
        // e.g winnerTable[5] contains the entries with 5 matches
        // maybe we should use a map, but this is more straightforward
        int[] winnerTable = new int[config.getNumberOfNumbers() + 1];
        for (LotteryEntry lotteryEntry : winners) {
            int numberOfMatches = winnerNumbers.equalEntries(lotteryEntry);
            winnerTable[numberOfMatches]++;
        }
        return winnerTable;
    }

    /**
     * Creates a human readable winners table
     *
     * @param winnerTable array with the number of winners
     * @return String ready for human consumption. :)
     */
    private StringBuilder createHumanReadableWinnersTable(int[] winnerTable) {
        StringBuilder outputString = new StringBuilder("Numbers Matching    Winners");
        outputString.append(System.getProperty("line.separator"));
        for (int numberOfMatches = config.getNumberOfNumbers();
             numberOfMatches >= config.getMinimumMatches();
             numberOfMatches--) {
            outputString.append(String.format("%d                   %d%s",
                    numberOfMatches,
                    winnerTable[numberOfMatches], System.getProperty("line.separator")));
        }
        return outputString;
    }

    /**
     * If user enters exit, quit, stop or a simple dot, the program will stop.
     * The Ctrl-C is handled, as well, this is a kind of a luxury here.
     *
     * @param line line to be checked for the signs of user trying to escape
     * @return true if the program should stop
     */
    private boolean userWantsToExit(String line) {
        return null == line || line.toLowerCase().matches("(exit)|(quit)|(stop)|(\\.)");
    }

    /**
     * Prints the prompt to the console
     */
    private void printPromptToConsole() {
        System.out.print("Ready> ");
        System.out.flush();
    }

    /**
     * Setter for the lotteryConfig
     *
     * @param config The LotteryConfig
     */
    public void setLotteryConfig(LotteryConfig config) {
        this.config = config;
        LotteryEntryBuilder.setLotteryConfig(config);
    }

    /**
     * This is the way we handle Ctrl-C from the console.
     * Since Java 8, every Java program shall contain at least one lambda expression. Just kidding.
     */
    public void setupInterruptHandler() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> running = false));
    }
}