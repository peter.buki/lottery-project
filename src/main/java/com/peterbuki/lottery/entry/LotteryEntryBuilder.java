package com.peterbuki.lottery.entry;

import com.peterbuki.lottery.main.LotteryConfig;

import java.util.*;

/**
 * Builder class for the LotteryEntry.
 * The builder logic was separated so that the LotteryEntry stays clean.
 * In addition, it allows the validation and building process to be more complicated,
 * for example the entry can be built in more steps - not used currently.
 */
public class LotteryEntryBuilder {
    private static LotteryConfig lotteryConfig;

    public static void setLotteryConfig(LotteryConfig config) {
        lotteryConfig = config;
    }

    /**
     * Creates a LotteryEntry from a line
     *
     * @param line Numbers separated by space, e.g. "1 2 3 4 5"
     * @return LotteryEntry
     * @throws LotteryException if not enough or too many or invalid numbers provided
     */
    public static LotteryEntry fromString(String line) throws LotteryException {
        Set<Integer> set = new HashSet<>();
        Scanner scanner = new Scanner(line);
        while (scanner.hasNext()) {
            try {
                int nextInt = scanner.nextInt();
                if ( ! verifyNumber(nextInt) || ! set.add(nextInt)) {
                    throw new LotteryException("Invalid input: " + line);
                }
            }
            catch (InputMismatchException e) {
                throw new LotteryException("Invalid input: " + line);
            }
        }
        if (set.size() != lotteryConfig.getNumberOfNumbers()) {
            throw new LotteryException("Invalid input: " + line);
        }
        return new LotteryEntry(set);

    }

    /**
     * Verifies if the number is within range.
     *
     * @param numberToBeVerified a single number to be checked
     * @return true if number is in range, false otherwise
     */
    private static boolean verifyNumber(int numberToBeVerified) {
        return lotteryConfig.getMinimumValue() <= numberToBeVerified &&
                numberToBeVerified <= lotteryConfig.getMaximumValue();
    }

    /**
     * Supports creating LotteryEntry from an array.
     *
     * @param numbers to be used for the entry
     * @return LotteryEntry upon successful validation
     * @throws LotteryException if not enough, too many or invalid numbers provided
     */
    public static LotteryEntry fromArray(Integer... numbers) throws LotteryException {
        Arrays.sort(numbers);
        if (numbers.length != lotteryConfig.getNumberOfNumbers()) {
            throw new LotteryException("Invalid input: " + Arrays.toString(numbers));
        }
        // The array is sorted, so if the first is higher than the minimum
        // and the last is lower than the maximum, it's ok
        if (!verifyNumber(numbers[0]) || !verifyNumber(numbers[numbers.length - 1])) {
            throw new LotteryException("Invalid input: " + Arrays.toString(numbers));
        }
        return new LotteryEntry(numbers);
    }

}
