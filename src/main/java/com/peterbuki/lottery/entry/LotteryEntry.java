package com.peterbuki.lottery.entry;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * A holder for the numbers
 */
public class LotteryEntry {
    private Set<Integer> set;

    /**
     * Default constructor hidden
     */
    private LotteryEntry() { }

    /**
     * Constructor for array based creation of the object
     *
     * @param numbers The numbers for the entry
     */
    protected LotteryEntry(Integer... numbers) {
        this(new HashSet<>(Arrays.asList(numbers)));
    }

    /**
     * Constructor for set based creation
     *
     * @param numbers of the entry
     */
    protected LotteryEntry(Set<Integer> numbers) {
        this();
        set = numbers;
    }

    /**
     * Returns the numbers as an array
     *
     * @return Integer[] numbers: the numbers in the entry
     */
    public Integer[] getNumbers() {
        Integer numbers[] = new Integer[set.size()];
        set.toArray(numbers);
        return numbers;
    }

    /**
     * Returns the entry in a human readable format
     *
     * @return String the entry "Entry: 1 2 3 4 5"
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Entry: ");
        for (Integer number : getNumbers()) {
            stringBuilder.append(number);
            stringBuilder.append(" ");
        }
        // chop the space from the end of the string
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    /**
     * Returns true if the argument is the same entry or contains the same number
     * Returns false otherwise
     *
     * @param o LotteryEntry to be compared
     * @return boolean value, see above
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LotteryEntry)) {
            return false;
        }
        return equals((LotteryEntry) o);
    }

    /**
     * Returns true if the same numbers are in the entry
     * Returns false if the numbers differ
     *
     * @param entry to be compared to
     * @return true if the numbers are the same
     */
    public boolean equals(LotteryEntry entry) {
        return set.equals(entry.set);
    }

    /**
     * Returns the number of same numbers in the two entries
     *
     * @param entry to be compared to, e.g. winningEntry
     * @return number of numbers present in both entries
     */
    public int equalEntries(LotteryEntry entry) {
        int result = 0;
        for (Integer number : set) {
            if (entry.set.contains(number)) {
                result++;
            }
        }
        return result;
    }
}
