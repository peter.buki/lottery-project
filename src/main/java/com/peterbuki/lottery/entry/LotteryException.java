package com.peterbuki.lottery.entry;

/**
 * An exception - currently used when invalid entry is to be created.
 */
public class LotteryException extends Exception {

    private final String message;

    public LotteryException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
