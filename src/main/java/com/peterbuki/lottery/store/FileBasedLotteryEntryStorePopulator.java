package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;
import com.peterbuki.lottery.entry.LotteryEntryBuilder;
import com.peterbuki.lottery.entry.LotteryException;
import com.peterbuki.lottery.main.LotteryConfig;
import com.peterbuki.lottery.util.ProgressBar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Responsible for populating the store with entries from a file.
 */
public class FileBasedLotteryEntryStorePopulator implements LotteryEntryStorePopulator {
    private final File file;
    private BufferedReader bufferedReader;
    private LotteryConfig config;
    private ProgressBar progressBar;

    /**
     * Constructor for a File
     *
     * @param file containing the entries
     * @throws IOException if file does not exist or not readable
     */
    private FileBasedLotteryEntryStorePopulator(File file) throws IOException {
        this.file = file;
        if (!file.exists()) {
            throw new IOException(String.format("File '%s' is missing.", file.getName()));
        }
        if (!file.canRead()) {
            throw new IOException(String.format("File '%s' is is not readable.", file.getName()));
        }
        bufferedReader = new BufferedReader(new FileReader(file));
        progressBar = new ProgressBar(System.out);
        progressBar.setMaxStatus(file.length());
    }

    @Override
    public void setLotteryConfig(LotteryConfig lotteryConfig) {
        this.config = lotteryConfig;
    }

    /**
     * Constructor for filename
     *
     * @param filename the name of the file to be used
     * @throws IOException if the file does not exist or not readable
     */
    public FileBasedLotteryEntryStorePopulator(String filename) throws IOException {
        this(new File(filename));
    }

    /**
     * Reads the entries from the file and stores in the store
     *
     * @param store to be used for storing entries
     * @throws IOException if there was a problem with reading the file
     */
    public void readLotteryEntriesFromFile(LotteryEntryStore store) throws IOException {
        progressBar.start();
        long position = 0;
        for (String line; (line = bufferedReader.readLine()) != null; ) {
            try {
                LotteryEntry entry = LotteryEntryBuilder.fromString(line);
                store.addEntry(entry);
            } catch (LotteryException rte) {
                System.err.println("Warning: skipping invalid entry: " + line);
            }
            position += line.getBytes().length + 1;
            progressBar.setCurrentStatus(position);
        }
        progressBar.setCurrentStatus(file.length());
        progressBar.stop();
    }
}
