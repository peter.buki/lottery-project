package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;

import java.util.List;

public interface LotteryEntryStore {
    public void addEntry(LotteryEntry lotteryEntry);

    public void addEntries(LotteryEntry[] lotteryEntries);

    public List<LotteryEntry> findMatchingEntries(LotteryEntry lotteryEntry, int minimumMatches);

    public long getStoredRecordCount();
}
