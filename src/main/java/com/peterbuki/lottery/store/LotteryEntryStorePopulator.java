package com.peterbuki.lottery.store;

import com.peterbuki.lottery.main.LotteryConfig;

import java.io.IOException;

public interface LotteryEntryStorePopulator {
    public void setLotteryConfig(LotteryConfig lotteryConfig);

    public void readLotteryEntriesFromFile(LotteryEntryStore store) throws IOException;
}
