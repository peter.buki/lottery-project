package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;

public abstract class AbstractLotteryEntryStore implements LotteryEntryStore {
    @Override
    public void addEntries(LotteryEntry[] lotteryEntries) {
        for (LotteryEntry lotteryEntry : lotteryEntries)
            addEntry(lotteryEntry);
    }
}
