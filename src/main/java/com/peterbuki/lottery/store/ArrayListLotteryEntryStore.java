package com.peterbuki.lottery.store;

import com.peterbuki.lottery.entry.LotteryEntry;

import java.util.ArrayList;
import java.util.List;

public class ArrayListLotteryEntryStore extends AbstractLotteryEntryStore {
    List<LotteryEntry> store = new ArrayList<>();
    private Long numberOfEntries = 0L;

    public Long getNumberOfEntries() {
        return numberOfEntries;
    }

    @Override
    public void addEntry(LotteryEntry lotteryEntry) {
        store.add(lotteryEntry);
        numberOfEntries++;
    }


    @Override
    public List<LotteryEntry> findMatchingEntries(LotteryEntry lotteryEntry, int minimumMatches) {
        List<LotteryEntry> result = new ArrayList<>();
        for (LotteryEntry candidate : store) {
            if (lotteryEntry.equalEntries(candidate) >= minimumMatches) {
                result.add(candidate);
            }
        }
        return result;
    }

    @Override
    public long getStoredRecordCount() {
        return store.size();
    }
}
