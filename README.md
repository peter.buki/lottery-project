# Lottery Project

<h2>Description</h2>

The program supports the lottery draw process by telling the number of winning entries.
The file shall contain lottery ticket entries, one entry per line, each with 5 numbers between 1 and 90.
The invalid entries will be ignored with a "Warning: skipping invalid entry -2 4 32 52 91" message.

Example structure for the input file:
<pre>
1 2 3 4 5
90 12 34 1 39
</pre>

After reading up the file, the Ready> prompt will show up.
At this point the program is ready for taking winning numbers.

<h2>Building the project</h2>
You will need to have git and gradle installed in order to build.

1. Clone the project:
<pre>
localhost:~$ git clone https://gitlab.com/peter.buki/lottery-project.git
</pre>

2. Enter the directory:
<pre>
localhost:~$ cd lottery-project
localhost:~/lottery-project$
</pre>

3. Build the project using gradle:
<pre>
localhost:~/lottery-project$ gradle build
> Task :compileJava UP-TO-DATE
> Task :processResources NO-SOURCE
> Task :classes UP-TO-DATE
> Task :jar UP-TO-DATE
> Task :startScripts UP-TO-DATE
> Task :distTar UP-TO-DATE
> Task :distZip UP-TO-DATE
> Task :assemble UP-TO-DATE
> Task :compileTestJava UP-TO-DATE
> Task :processTestResources UP-TO-DATE
> Task :testClasses UP-TO-DATE
> Task :test UP-TO-DATE
> Task :check UP-TO-DATE
> Task :build UP-TO-DATE

BUILD SUCCESSFUL in 3s
8 actionable tasks: 8 up-to-date
</pre>

<h2>Executing the program</h2>
You can execute the program with java.
<pre>
 localhost:~/lottery-project$ java -jar build/libs/lottery-project.jar 100k-entries.txt
 [#########################################################################] 100%
 Loaded 100000 entries in 3738ms, that's 26752 entry/seconds!
 Ready> 8 54 34 70 89
 Found 2222 matching entries in 19ms, that's 5263157 entry/seconds.
 Numbers Matching    Winners
 5                   1
 4                   1
 3                   88
 2                   2153
 Ready> exit
 localhost:~/lottery-project$
</pre>
You can exit any time using Ctrl-C or entering 'exit' at the prompt.

<p>
You can also run the program without verbose logging:
<pre>
 localhost:~/lottery-project$ java -jar build/libs/lottery-project.jar 100k-entries.txt 2>/dev/null
 [#########################################################################] 100%
 Ready> 8 54 34 70 89
 Numbers Matching    Winners
 5                   1
 4                   1
 3                   88
 2                   2153
 Ready> exit
 localhost:~/lottery-project$
</pre>


<h2>Performance measurements</h2>

Performance measurements for loading entries
<pre>
Read 10k records in 441ms (~1 sec), 22675 records/seconds, memory usage: 427MB
Read 100k records in 2207ms (~2 sec), 45310 records/seconds, memory usage: 1479MB
Read 1M records in 18027ms (~18 sec), 55472 records/seconds, memory usage: 1781MB
Read 5M records in 94728ms (~1.5 min), 52782 records/seconds, memory usage: 2902MB
Read 10M records in 424540ms (~7 min), 23554 records/seconds, memory usage: 3968MB
</pre>

Performance measurements for searching entries
<pre>
Searched through 10k records in 5-10ms (~0 sec)
Searched through 100k records in 20-50ms (~0 sec)
Searched through 1M records in 15-200ms (~0 sec)
Searched through 5M records in 700-800ms (~1 sec)
Searched through 10M records in ms 1300-1800ms (~2 sec)
</pre>
